package models;

import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;

public class Case {
    private Agent agent;
    private final Circle circle;
    private final Rectangle background;
    private final Label label;

    public Case(Agent agent) {
        this.agent = agent;

        this.circle = new Circle(25);
        this.circle.setFill(Color.TRANSPARENT);

        this.background = new Rectangle();
        this.background.setHeight(100.0);
        this.background.setWidth(100.0);
        this.background.setFill(Color.TRANSPARENT);

        this.label = new Label("Ok !");
        this.label.setTextFill(Color.TRANSPARENT);
    }

    public Case() {
        this(null);
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public Circle getCircle() {
        return circle;
    }

    public Rectangle getBackground() {
        return background;
    }

    public Label getLabel() {
        return label;
    }

    /**
     * Met à jour l'interface graphique de la case
     * - couleur du cercle en fonction de l'agent
     * - mise en place du label si l'agent est sur sa case finale
     *
     * le background ne change jamais : il permet de savoir où doit aller un agent
     */
    public void update() {
        if (this.agent != null) {
            this.circle.setFill(this.agent.getColor());
            if (this.agent.getPosAct().equals(this.agent.getPosFin()))
                if (this.agent.getColor() == Color.BLACK)
                    this.label.setTextFill(Color.WHITE);
                else
                    this.label.setTextFill(Color.BLACK);
            else
                this.label.setTextFill(Color.TRANSPARENT);
        } else {
            this.circle.setFill(Color.TRANSPARENT);
            this.label.setTextFill(Color.TRANSPARENT);
        }
    }
}
