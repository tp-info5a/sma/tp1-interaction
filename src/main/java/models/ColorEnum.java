package models;

import javafx.scene.paint.Color;

public enum ColorEnum {
    AQUA(Color.AQUA),
    BLUE(Color.BLUE),
    BLACK(Color.BLACK),
    BLUEVIOLET(Color.BLUEVIOLET),
    CRIMSON(Color.CRIMSON),
    DARKCYAN(Color.DARKCYAN),
    DARKGOLDENROD(Color.DARKGOLDENROD),
    DARKGREY(Color.DARKGREY),
    DARKMAGENTA(Color.DARKMAGENTA),
    DARKOLIVEGREEN(Color.DARKOLIVEGREEN),
    DARKORANGE(Color.DARKORANGE),
    DARKRED(Color.DARKRED),
    DARKSALMON(Color.DARKSALMON),
    DARKSEAGREEN(Color.DARKSEAGREEN),
    DARKSLATEBLUE(Color.DARKSLATEBLUE),
    DEEPPINK(Color.DEEPPINK),
    GREEN(Color.GREEN),
    GREY(Color.GREY),
    LAWNGREEN(Color.LAWNGREEN),
    LIGHTSEAGREEN(Color.LIGHTSEAGREEN),
    ORANGERED(Color.ORANGERED),
    RED(Color.RED),
    YELLOW(Color.YELLOW),
    YELLOWGREEN(Color.YELLOWGREEN);



    private Color color;

    ColorEnum(Color color) {
        this.color = color;
    }

    public Color getColor() {
        return color;
    }
}
