package models;

public enum DirectionEnum {
    NORD(-1, 0),
    SUD(1, 0),
    EST(0, 1),
    OUEST(0, -1);

    private Integer ligne;
    private Integer colonne;

    DirectionEnum(Integer ligne, Integer colonne) {
        this.ligne = ligne;
        this.colonne = colonne;
    }

    public Integer getLigne() {
        return ligne;
    }

    public Integer getColonne() {
        return colonne;
    }
}
