package models;

import java.time.LocalDateTime;

public class Message {
    private final Agent emetteur;
    private final Agent destinataire;
    private final LocalDateTime dateTime;
    private final Request request;
    private final Position[] position;

    public Message(Agent emetteur, Agent destinataire, Request request, Position[] position) {
        this.emetteur = emetteur;
        this.destinataire = destinataire;
        this.dateTime = LocalDateTime.now();
        this.request = request;
        this.position = position;
    }

    public Agent getEmetteur() {
        return emetteur;
    }

    public Agent getDestinataire() {
        return destinataire;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public Request getRequest() {
        return request;
    }

    public Position[] getPosition() {
        return position;
    }
}
