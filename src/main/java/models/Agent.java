package models;

import exceptions.AppliException;
import javafx.scene.paint.Color;
import utils.AStarSearch;
import utils.RandomSearch;

import java.util.*;
import java.util.concurrent.Semaphore;

public class Agent implements Runnable{
    private final Semaphore semaphore;
    private Position posAct;
    private Position posFin;
    private final Environment env;
    private final Color color;
    private final List<Message> postBox;
    private final Boolean communicationAgent;
    private Integer priority;
    private Agent lastEmetteur;

    public Agent(Semaphore semaphore, Position posAct, Environment env, Color color, Boolean communicationAgent) {
        this.semaphore = semaphore;
        this.posAct = posAct;
        this.env = env;
        this.color = color;
        this.postBox = new ArrayList<>();
        this.communicationAgent = communicationAgent;
        this.lastEmetteur = null;
    }

    public Position getPosAct() {
        return posAct;
    }

    public void setPosAct(Position posAct) {
        this.posAct = posAct;
    }

    public List<Message> getPostBox() { return postBox; }

    public Position getPosFin() {
        return posFin;
    }

    public void setPosFin(Position posFin) {
        this.posFin = posFin;
    }

    public Color getColor() {
        return color;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public Agent getLastEmetteur() {
        return lastEmetteur;
    }

    /**
     * Déplacement l'agent sur un position donnée (index : 0)
     * le 2ème index de chemin sert au message pour dire où veux aller l'agent
     * @param chemin les deux prochain déplacements de l'agent
     * @return booléen permettant de savoir si l'agent à pu bouger ou pas
     */
    public Boolean move(Position[] chemin){
        List<List<Case>> grille = this.env.getGrid();
        Agent agent = grille.get(chemin[0].getLigne()).get(chemin[0].getColonne()).getAgent();
        if(chemin[0].equals(posAct)){
            return true;
        } else if(agent == null){
            this.env.move(this, chemin[0]);
            return true;
        } else if(this.communicationAgent && agent != this.lastEmetteur) {
            Message message = new Message(this,agent,Request.MOVE,chemin);
            agent.post(message);
            return false;
        } else if (agent == this.lastEmetteur) {
            this.lastEmetteur = null;
        }
        return false;
    }

    /**
     * Booléen permetant de savoir si l'agant est sur sa case finale
     * @return true si l'agant est sur sa case finale, false sinon
     */
    public Boolean done() {
        return this.posAct.equals(this.posFin);
    }

    public Position[] newPosWithRestriction(Message message){
        Map<Position, Case> positionCaseMap = this.env.voirVoisinnage(this.posAct);

        Optional<Position> optionalPosition0 = positionCaseMap.keySet().stream().filter(position -> position.equals(message.getEmetteur().getPosAct())).findAny();
        Optional<Position> optionalPosition1 = positionCaseMap.keySet().stream().filter(position -> position.equals(message.getPosition()[1])).findAny();

        optionalPosition0.ifPresent(positionCaseMap::remove);
        optionalPosition1.ifPresent(positionCaseMap::remove);

        Optional<Map.Entry<Position, Case>> caseWithoutAgent = positionCaseMap.entrySet()
                .stream()
                .filter(positionCaseEntry -> positionCaseEntry.getValue().getAgent() == null)
                .sorted(Comparator.comparing(positionCaseEntry -> positionCaseEntry.getKey().distance(this.posFin)))
                .findAny();


        if (caseWithoutAgent.isPresent())
            return new Position[] {caseWithoutAgent.get().getKey(), message.getPosition()[1]};

        Optional<Map.Entry<Position, Case>> caseAgentNotDone = positionCaseMap.entrySet()
                .stream()
                .filter(positionCaseEntry -> !positionCaseEntry.getValue().getAgent().done())
                .findAny();
        if (caseAgentNotDone.isPresent())
            return new Position[] {caseAgentNotDone.get().getKey(), message.getPosition()[1]};

        Optional<Map.Entry<Position, Case>> first = positionCaseMap.entrySet()
                .stream()
                .sorted(Comparator.comparing(positionCaseEntry -> positionCaseEntry.getValue().getAgent().getPriority()))
                .findFirst();

        return first.map(positionCaseEntry -> new Position[]{positionCaseEntry.getKey(), message.getPosition()[1]}).orElseGet(message::getPosition);

    }

    public Position[] moveAsked(Position[] direction){
        Optional<Message> max = this.postBox.stream()
                .filter(message -> message.getPosition()[0].equals(this.posAct))
                .max(Comparator.comparing(message -> message.getEmetteur().getPriority()));

        this.postBox.clear();
        if (max.isEmpty())
            return direction;

        this.lastEmetteur = max.get().getEmetteur();
        return this.newPosWithRestriction(max.get());
    }

    public void post(Message message){
        this.postBox.add(message);
    }

    @Override
    public void run() {
        while (!env.done() && !Thread.currentThread().isInterrupted()) {
            try {
                if (!this.done() || !this.postBox.isEmpty()) {
                    this.semaphore.acquire();
                    System.out.print(this.toString() + " | semaphore.acquire()");
                    Position[] chemin = null;
                    try {
                        chemin = AStarSearch.algo(env.getGrid(), this, false);
                    } catch (AppliException e) {
                        if (this.communicationAgent)
                            chemin = AStarSearch.algo(env.getGrid(), this, true);
                        else
                            chemin = RandomSearch.algo(env.getGrid(), this);
                    }
                    if (!this.postBox.isEmpty()) chemin = moveAsked(chemin);
                    if (chemin != null) this.move(chemin);

                    Thread.sleep(50);

                    System.out.println(" | semaphore.release() -> ok");
                    this.semaphore.release();
                } else {
                    Thread.sleep(50);
                }
            } catch (AppliException e) {
                System.out.println(" | semaphore.release() -> AppliException");
                this.semaphore.release();
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
    }

    @Override
    public String toString() {
        return "Agent{" +
                "posAct=" + posAct +
                ", posFin=" + posFin +
                '}';
    }
}
