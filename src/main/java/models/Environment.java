package models;

import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;

import java.util.*;
import java.util.concurrent.Semaphore;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Environment {
    private final Integer n;
    private final List<Agent> agentList;
    private final List<List<Case>> grid;

    private final int[][] priority = {
            {5, 4, 3, 4, 5},
            {4, 3, 2, 3, 4},
            {3, 2, 1, 2, 3},
            {4, 3, 2, 3, 4},
            {5, 4, 3, 4, 5},
    };

    // ancienne map des priorités
    /*private final int[][] priority = {
            {4, 2, 3, 2, 4},
            {2, 3, 3, 3, 2},
            {3, 3, 5, 3, 3},
            {2, 3, 3, 3, 2},
            {4, 2, 3, 2, 4}
    };*/

    public Environment(Integer n, Integer nbrAgent, Boolean communicationAgent) {
        /*
         * Création de la grille 2D (5x5) qui sevira de l'environnement de l'application
         */
        this.n = n;
        this.grid = new ArrayList<>();
        List<Case> caseList;
        for (int i = 0 ; i < n ; i++) {
            caseList = IntStream.range(0, n)
                    .mapToObj((int value) -> new Case())
                    .collect(Collectors.toList());

            this.grid.add(caseList);
        }

        /*
         * Création des agents sur une case aléatoire
         * on verifie quand même que cette case est vide avant de mettre un agent sur un case
         */
        int iteration = 0;
        Random random = new Random();
        this.agentList = new ArrayList<>();
        Semaphore semaphore = new Semaphore(1, true);
        while (iteration < nbrAgent) {
            int i1 = random.nextInt(this.n);
            int i2 = random.nextInt(this.n);
            if (this.grid.get(i1).get(i2).getAgent() == null) {
                Agent agent = new Agent(semaphore, new Position(i1, i2), this, ColorEnum.values()[iteration].getColor(), communicationAgent);
                this.agentList.add(agent);
                this.grid.get(i1).get(i2).setAgent(agent);
                iteration++;
            }
        }

        /*
         * Mise en place de la position finale des agents
         * 1 - création d'une nouvelle grille vide
         * 2 - attribution d'une case aléatoire vide à un agent
         */
        List<List<Case>> finalGrid = new ArrayList<>();
        for (int i = 0 ; i < n ; i++) {
            caseList = IntStream.range(0, n)
                    .mapToObj((int value) -> new Case())
                    .collect(Collectors.toList());

            finalGrid.add(caseList);
        }

        for (Agent agent : agentList) {
            boolean next = false;
            do {
                int i = random.nextInt(this.n);
                int j = random.nextInt(this.n);
                if (finalGrid.get(i).get(j).getAgent() == null && i != agent.getPosAct().getLigne() && j != agent.getPosAct().getColonne()) {
                    finalGrid.get(i).get(j).setAgent(agent);
                    agent.setPosFin(new Position(i, j));
                    agent.setPriority(this.priority[i][j]);
                    this.grid.get(i).get(j).getBackground().setFill(agent.getColor());
                    next = true;
                }
            } while (!next);
        }
    }

    public List<Agent> getAgentList() {
        return agentList;
    }

    public List<List<Case>> getGrid() {
        return grid;
    }

    /**
     * Permet d'avoir les 4 (au plus) case voisines à une position donnée
     * @param position position pour laquelle regarder les voisin
     * @return une map entre une position est une case
     */
    public Map<Position, Case> voirVoisinnage(Position position) {
        Map<Position, Case> voisinnage = new HashMap<>();
        Integer ligne = position.getLigne();
        Integer colonne = position.getColonne();
        if (ligne > 0) voisinnage.put(new Position(ligne - 1, colonne), this.grid.get(ligne - 1).get(colonne));
        if (ligne < 4) voisinnage.put(new Position(ligne + 1, colonne), this.grid.get(ligne + 1).get(colonne));
        if (colonne > 0) voisinnage.put(new Position(ligne, colonne - 1), this.grid.get(ligne).get(colonne - 1));
        if (colonne < 4) voisinnage.put(new Position(ligne, colonne + 1), this.grid.get(ligne).get(colonne + 1));

        return voisinnage;
    }

    /**
     * Déplace un agent sur une position
     * @param agent agent à déplacer
     * @param position nouvelle positon de l'agent
     */
    public void move(Agent agent, Position position) {
        this.grid.get(position.getLigne()).get(position.getColonne()).setAgent(agent);
        this.grid.get(agent.getPosAct().getLigne()).get(agent.getPosAct().getColonne()).setAgent(null);
        agent.setPosAct(position);
    }

    /**
     * Met en place la version graphique de la grille de l'environnement
     * @param gridPane layout
     */
    public void setCasesCircleOnGridPane(GridPane gridPane) {
        for (int i = 0; i < this.n ; i++) {
            for (int j = 0; j <this.n ; j++) {
                StackPane stackPane = new StackPane();
                stackPane.getChildren().addAll(this.grid.get(i).get(j).getBackground(), this.grid.get(i).get(j).getCircle(), this.grid.get(i).get(j).getLabel());
                gridPane.add(stackPane, j, i, 1, 1);
            }
        }
    }

    /**
     * Met à jour l'interface graphique
     */
    public void updateUI() {
        this.grid.stream()
                .flatMap(Collection::stream)
                .forEach(Case::update);
    }

    /**
     * Permet de savoir si tous les agents sont à leur place finale
     * @return true si tous les agents sont à leur place finale, faux sinon
     */
    public Boolean done() {
        return this.agentList.stream().allMatch(Agent::done);
    }
}
