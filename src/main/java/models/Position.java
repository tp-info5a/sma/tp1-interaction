package models;

import java.util.Objects;

public class Position {
    private final Integer ligne;
    private final Integer colonne;

    public Position(Integer ligne, Integer colonne) {
        this.ligne = ligne;
        this.colonne = colonne;
    }

    public Integer getColonne() {
        return colonne;
    }

    public Integer getLigne() {
        return ligne;
    }

    /**
     * Distance de manhatan entre cette entité et goal
     * @param goal entité avec laquelle mesurer la distance
     * @return distance de manhatan entre les 2 entité
     */
    public Integer distance(Position goal){
        return Math.abs(ligne - goal.ligne) + Math.abs(colonne - goal.colonne);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Position position = (Position) o;
        return Objects.equals(ligne, position.ligne) &&
                Objects.equals(colonne, position.colonne);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ligne, colonne);
    }

    @Override
    public String toString() {
        return "(" +
                "l=" + ligne +
                ", c=" + colonne +
                ')';
    }
}
