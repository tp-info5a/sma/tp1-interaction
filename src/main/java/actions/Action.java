package actions;

import models.Environment;

import java.util.List;
import java.util.stream.Collectors;

public class Action implements Runnable{
    private final Environment env;

    public Action(Environment env) {
        this.env = env;
    }

    @Override
    public void run() {
        List<Thread> collect = this.env.getAgentList()
                .stream()
                .map(Thread::new)
                .collect(Collectors.toList());

        Thread thread = new Thread(() -> {
            while (!this.env.done() && !Thread.currentThread().isInterrupted()) {
                this.env.updateUI();
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
            }
            this.env.updateUI();
        });

        collect.add(thread);

        collect.forEach(Thread::start);

        while (!this.env.done() && !Thread.currentThread().isInterrupted()) {
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                System.out.println("Action stop.");
                Thread.currentThread().interrupt();
            }
        }

        collect.forEach(Thread::interrupt);
    }


}
