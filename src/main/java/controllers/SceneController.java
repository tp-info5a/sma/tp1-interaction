package controllers;

import actions.Action;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import models.ColorEnum;
import models.Environment;

public class SceneController {
    private final Stage variableStage;
    private final Stage primaryStage;
    private Thread thread;

    public SceneController(Stage variableStage, Stage primaryStage) {
        this.variableStage = variableStage;
        this.primaryStage = primaryStage;
    }

    public Thread getThread() {
        return thread;
    }

    public void selectVariable() {
        GridPane gridPane = new GridPane();
        gridPane.setPadding(new Insets(5));
        gridPane.setHgap(5);
        gridPane.setVgap(5);

        Label labelGrille = new Label("Dimension de la grille :");
        gridPane.add(labelGrille, 0, 0);

        TextField textFieldGrille = new TextField();
        textFieldGrille.setText("5");
        textFieldGrille.setEditable(false);
        gridPane.add(textFieldGrille, 1, 0);

        Label labelAgent = new Label("Nombre d'agent :");
        gridPane.add(labelAgent, 0, 1);

        TextField textFieldAgent = new TextField();
        textFieldAgent.setText("1");
        textFieldAgent.setEditable(false);
        gridPane.add(textFieldAgent, 1, 1);

        Button btnMinus = new Button("-");
        HBox hbBtnMinus = new HBox(10);
        hbBtnMinus.setAlignment(Pos.BOTTOM_CENTER);
        hbBtnMinus.getChildren().add(btnMinus);
        gridPane.add(hbBtnMinus, 2, 1, 1, 1);

        btnMinus.setOnAction(e -> {
            int nbr = Integer.parseInt(textFieldAgent.getText());
            if (nbr > 0)
                textFieldAgent.setText(Integer.toString(nbr - 1));
        });

        Button btnPlus = new Button("+");
        HBox hbBtnPlus = new HBox(10);
        hbBtnPlus.setAlignment(Pos.BOTTOM_CENTER);
        hbBtnPlus.getChildren().add(btnPlus);
        gridPane.add(hbBtnPlus, 3, 1, 1, 1);

        btnPlus.setOnAction(e -> {
            int nbr = Integer.parseInt(textFieldAgent.getText());
            if (nbr < ColorEnum.values().length)
                textFieldAgent.setText(Integer.toString(nbr + 1));
        });

        CheckBox checkBoxCommuncation = new CheckBox("Communication entre agent");
        gridPane.add(checkBoxCommuncation, 0, 2, 2, 1);

        Button btnInit = new Button("Initialiser l'environnement");
        HBox hbBtnInit = new HBox(10);
        hbBtnInit.setAlignment(Pos.BOTTOM_CENTER);
        hbBtnInit.getChildren().add(btnInit);
        gridPane.add(hbBtnInit, 0, 3, 2, 1);

        Button btnAlgo = new Button("Lancer l'algo");
        btnAlgo.setDisable(true);
        HBox hbBtnAlgo = new HBox(10);
        hbBtnAlgo.setAlignment(Pos.BOTTOM_CENTER);
        hbBtnAlgo.getChildren().add(btnAlgo);
        gridPane.add(hbBtnAlgo, 0, 4, 2, 1);

        Button btnStop = new Button("Stop");
        btnStop.setDisable(true);
        HBox hbBtnStop = new HBox(10);
        hbBtnStop.setAlignment(Pos.BOTTOM_CENTER);
        hbBtnStop.getChildren().add(btnStop);
        gridPane.add(hbBtnStop, 0, 5, 2, 1);

        btnInit.setOnAction(e -> {
            if (this.primaryStage != null) this.primaryStage.close();
            this.initEnv(
                    Integer.parseInt(textFieldGrille.getText()),
                    Integer.parseInt(textFieldAgent.getText()),
                    checkBoxCommuncation.isSelected()
            );

            btnAlgo.setDisable(false);
        });

        btnAlgo.setOnAction(e -> {
            if (this.thread != null)
                this.thread.start();

            btnInit.setDisable(true);
            btnAlgo.setDisable(true);
            btnPlus.setDisable(true);
            btnMinus.setDisable(true);
            btnStop.setDisable(false);
            checkBoxCommuncation.setDisable(true);
        });

        btnStop.setOnAction(e -> {
            if (this.thread != null)
                this.thread.interrupt();

            btnInit.setDisable(false);
            btnStop.setDisable(true);
            btnPlus.setDisable(false);
            btnMinus.setDisable(false);
            checkBoxCommuncation.setDisable(false);
        });

        this.variableStage.setScene(new Scene(gridPane, 400, 200));
        this.variableStage.show();
    }

    public void initEnv(int grille, int agent, boolean communication) {
        GridPane gridPaneAlgo = new GridPane();
        gridPaneAlgo.setStyle("-fx-background-color: white; -fx-grid-lines-visible: true");
        Environment environment = new Environment(grille, agent, communication);

        environment.setCasesCircleOnGridPane(gridPaneAlgo);
        environment.updateUI();

        Action action = new Action(environment);

        if (this.thread != null) {
            this.thread.interrupt();
        }

        this.thread = new Thread(action);

        this.primaryStage.setScene(new Scene(gridPaneAlgo, 500, 500));
        this.primaryStage.show();
    }
}
