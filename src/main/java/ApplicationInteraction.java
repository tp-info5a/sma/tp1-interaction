import controllers.SceneController;
import javafx.application.Application;
import javafx.stage.Stage;

public class ApplicationInteraction extends Application {
    private SceneController sceneController;

    @Override
    public void start(Stage primaryStage) throws Exception {
        Stage variableStage = new Stage();
        variableStage.setTitle("Variable");

        primaryStage.setTitle("Interaction multi agents");

        this.sceneController = new SceneController(variableStage, primaryStage);
        this.sceneController.selectVariable();
    }

    @Override
    public void stop() throws Exception {
        super.stop();
        if (this.sceneController.getThread() != null) {
            this.sceneController.getThread().interrupt();
        }
    }

    public static void startApplication(String[] args) {
        launch(args);
    }
}
