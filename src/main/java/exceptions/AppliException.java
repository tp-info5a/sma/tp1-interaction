package exceptions;

public class AppliException extends Exception{
    public AppliException(String message) {
        super(message);
    }
}
