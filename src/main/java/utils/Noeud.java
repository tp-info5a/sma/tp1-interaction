package utils;

import models.Position;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Noeud {
    private Position position;
    private Integer cout;
    private Integer heuristique;
    private List<Noeud> connectionList;
    private Noeud predecesseur;
    private Boolean hasAgent;

    public Noeud(Position position, Integer cout, Integer heuristique, Boolean hasAgent) {
        this.position = position;
        this.cout = cout;
        this.heuristique = heuristique;
        this.connectionList = new ArrayList<>();
        this.predecesseur = null;
        this.hasAgent = hasAgent;
    }

    public Noeud(Position position, Boolean hasAgent) {
        this(position, Integer.MAX_VALUE, 0, hasAgent);
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Integer getCout() {
        return cout;
    }

    public void setCout(Integer cout) {
        this.cout = cout;
    }

    public Integer getHeuristique() {
        return heuristique;
    }

    public void computeHeuristique(Noeud destination) {
        this.heuristique = Math.abs(this.position.getLigne() - destination.position.getLigne())
                         + Math.abs(this.position.getColonne() - destination.position.getColonne());
    }

    public Integer getScore() {
        return this.cout + this.heuristique;
    }

    public List<Noeud> getConnectionList() {
        return connectionList;
    }

    public void addConnection(Noeud noeud) {
        this.connectionList.add(noeud);
    }

    public Noeud getPredecesseur() {
        return predecesseur;
    }

    public void setPredecesseur(Noeud predecesseur) {
        this.predecesseur = predecesseur;
    }

    public Boolean getHasAgent() {
        return hasAgent;
    }

    public void setHasAgent(Boolean hasAgent) {
        this.hasAgent = hasAgent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Noeud noeud = (Noeud) o;
        return Objects.equals(position, noeud.position) && Objects.equals(cout, noeud.cout) && Objects.equals(heuristique, noeud.heuristique) && Objects.equals(connectionList, noeud.connectionList) && Objects.equals(predecesseur, noeud.predecesseur) && Objects.equals(hasAgent, noeud.hasAgent);
    }

    @Override
    public int hashCode() {
        return Objects.hash(position, cout, heuristique, connectionList, predecesseur, hasAgent);
    }
}
