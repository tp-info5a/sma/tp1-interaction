package utils;

import exceptions.AppliException;
import models.Agent;
import models.Case;
import models.DirectionEnum;
import models.Position;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class RandomSearch {
    private static Position getRandomNeighbor(List<List<Case>> grille, Position position) throws AppliException {
        Optional<DirectionEnum> optional = Arrays.stream(DirectionEnum.values())
                .filter(directionEnum -> position.getLigne() + directionEnum.getLigne() >= 0 && position.getLigne() + directionEnum.getLigne() < 5)
                .filter(directionEnum -> position.getColonne() + directionEnum.getColonne() >= 0 && position.getColonne() + directionEnum.getColonne() < 5)
                .filter(directionEnum -> grille.get(position.getLigne() + directionEnum.getLigne()).get(position.getColonne() + directionEnum.getColonne()).getAgent() == null)
                .findAny();

        if (optional.isEmpty())
            throw new AppliException("No neighbor");

        return new Position(position.getLigne() + optional.get().getLigne(), position.getColonne() + optional.get().getColonne());
    }

    public static Position[] algo(List<List<Case>> grille, Agent agent) throws AppliException {
        Position[] positions = new Position[2];
        positions[0] = getRandomNeighbor(grille, agent.getPosAct());
        positions[1] = getRandomNeighbor(grille, positions[0]);
        return positions;
    }
}
