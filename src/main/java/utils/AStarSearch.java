package utils;

import exceptions.AppliException;
import models.Agent;
import models.Case;
import models.Position;

import java.util.*;

public class AStarSearch {

    public AStarSearch() {}

    private static void connectionOrdreAleatoire(Integer i, Integer j, List<List<Noeud>> graph, List<Noeud> noeudList, List<List<Case>> grille, Boolean ignorerAgent) {
        Random random = new Random();
        int r = random.nextInt();
        if (r % 4 == 0) {
            if (i > 0 && (ignorerAgent || grille.get(i-1).get(j).getAgent() == null)) {
                noeudList.get(j).addConnection(graph.get(i-1).get(j));
            }
            if (i < 4 && (ignorerAgent || grille.get(i+1).get(j).getAgent() == null)) {
                noeudList.get(j).addConnection(graph.get(i+1).get(j));
            }
            if (j > 0 && (ignorerAgent || grille.get(i).get(j-1).getAgent() == null)) {
                noeudList.get(j).addConnection(graph.get(i).get(j-1));
            }
            if (j < 4 && (ignorerAgent || grille.get(i).get(j+1).getAgent() == null)) {
                noeudList.get(j).addConnection(graph.get(i).get(j+1));
            }
        } else if (r % 4 == 1) {
            if (i < 4 && (ignorerAgent || grille.get(i+1).get(j).getAgent() == null)) {
                noeudList.get(j).addConnection(graph.get(i+1).get(j));
            }
            if (j > 0 && (ignorerAgent || grille.get(i).get(j-1).getAgent() == null)) {
                noeudList.get(j).addConnection(graph.get(i).get(j-1));
            }
            if (j < 4 && (ignorerAgent || grille.get(i).get(j+1).getAgent() == null)) {
                noeudList.get(j).addConnection(graph.get(i).get(j+1));
            }
            if (i > 0 && (ignorerAgent || grille.get(i-1).get(j).getAgent() == null)) {
                noeudList.get(j).addConnection(graph.get(i-1).get(j));
            }
        } else if (r % 4 == 2) {
            if (j > 0 && (ignorerAgent || grille.get(i).get(j-1).getAgent() == null)) {
                noeudList.get(j).addConnection(graph.get(i).get(j-1));
            }
            if (j < 4 && (ignorerAgent || grille.get(i).get(j+1).getAgent() == null)) {
                noeudList.get(j).addConnection(graph.get(i).get(j+1));
            }
            if (i > 0 && (ignorerAgent || grille.get(i-1).get(j).getAgent() == null)) {
                noeudList.get(j).addConnection(graph.get(i-1).get(j));
            }
            if (i < 4 && (ignorerAgent || grille.get(i+1).get(j).getAgent() == null)) {
                noeudList.get(j).addConnection(graph.get(i+1).get(j));
            }
        } else {
            if (j < 4 && (ignorerAgent || grille.get(i).get(j+1).getAgent() == null)) {
                noeudList.get(j).addConnection(graph.get(i).get(j+1));
            }
            if (i > 0 && (ignorerAgent || grille.get(i-1).get(j).getAgent() == null)) {
                noeudList.get(j).addConnection(graph.get(i-1).get(j));
            }
            if (i < 4 && (ignorerAgent || grille.get(i+1).get(j).getAgent() == null)) {
                noeudList.get(j).addConnection(graph.get(i+1).get(j));
            }
            if (j > 0 && (ignorerAgent || grille.get(i).get(j-1).getAgent() == null)) {
                noeudList.get(j).addConnection(graph.get(i).get(j-1));
            }
        }
    }

    private static List<List<Noeud>> convertion(List<List<Case>> grille, Boolean ignorerAgent) {
        List<List<Noeud>> graph = new ArrayList<>();
        for (int i = 0; i < grille.size(); i++) {
            List<Case> caseList = grille.get(i);
            List<Noeud> noeudList = new ArrayList<>();
            for (int j = 0; j < caseList.size(); j++) {
                noeudList.add(new Noeud(new Position(i, j), caseList.get(j).getAgent() != null));
            }
            graph.add(noeudList);
        }

        for (int i = 0; i < graph.size(); i++) {
            List<Noeud> noeudList = graph.get(i);
            for (int j = 0; j < noeudList.size(); j++) {
                connectionOrdreAleatoire(i, j, graph, noeudList, grille, ignorerAgent);
            }
        }

        return graph;
    }

    public static Optional<Noeud> getNoeudWithLowestScore(List<Noeud> openedList) {
        return openedList.stream().min(Comparator.comparing(Noeud::getScore));
    }

    public static Position[] getFirstSteps(Noeud noeud) {
        if (noeud.getPredecesseur() == null || noeud.getPredecesseur().getPredecesseur() == null) {
            return new Position[]{noeud.getPosition(), noeud.getPosition()};
        }

        Noeud second = noeud;
        Noeud first = noeud.getPredecesseur();
        while (first.getPredecesseur() != null && first.getPredecesseur().getPredecesseur() != null) {
            second = first;
            first = first.getPredecesseur();
        }

        return new Position[]{first.getPosition(), second.getPosition()};
    }

    public static Position[] algo(List<List<Case>> grille, Agent agent, Boolean ignorerAgent) throws AppliException {
        List<List<Noeud>> graph = convertion(grille, ignorerAgent);

        Noeud depart = graph.get(agent.getPosAct().getLigne()).get(agent.getPosAct().getColonne());
        depart.setCout(0);
        Noeud fin = graph.get(agent.getPosFin().getLigne()).get(agent.getPosFin().getColonne());

        if (ignorerAgent && agent.getLastEmetteur() != null && !agent.getLastEmetteur().getPosAct().equals(agent.getPosFin())) {
            Noeud noeudEmetteur = graph.get(agent.getLastEmetteur().getPosAct().getLigne()).get(agent.getLastEmetteur().getPosAct().getColonne());
            graph.stream()
                    .flatMap(Collection::stream)
                    .filter(noeud -> noeud.getConnectionList().contains(noeudEmetteur))
                    .forEach(noeud -> noeud.getConnectionList().remove(noeudEmetteur));

            noeudEmetteur.getConnectionList().clear();
        }

        graph.stream()
                .flatMap(Collection::stream)
                .forEach(noeud -> noeud.computeHeuristique(fin));

        List<Noeud> openedList = new ArrayList<>();
        openedList.add(depart);
        List<Noeud> closedList = new ArrayList<>();

        while (!openedList.isEmpty()) {
            Optional<Noeud> noeud = getNoeudWithLowestScore(openedList);
            if (noeud.isEmpty())
                throw new AppliException("Aucun chemin dispo");

            if (noeud.get().equals(fin)) {
                return getFirstSteps(fin);
            }

            for (Noeud connection : noeud.get().getConnectionList()) {
                if (!closedList.contains(connection)) {
                    int coutTmp = noeud.get().getCout();
                    if (connection.getHasAgent()) {
                        coutTmp += 10;
                    } else {
                        coutTmp++;
                    }
                    if (coutTmp < connection.getCout()) {
                        connection.setCout(coutTmp);
                        connection.setPredecesseur(noeud.get());
                        if (!openedList.contains(connection))
                            openedList.add(connection);
                    }
                }
            }
            closedList.add(noeud.get());
            openedList.remove(noeud.get());
        }

        throw new AppliException("Aucun chemin dispo");
    }
}
